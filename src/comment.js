import React from 'react';
import ReactDOM from 'react-dom';

function Avatar(props) {
  return (
    <img className="Avatar"
         src={props.user.avatarUrl}
         alt={props.user.name}
         />    
  );
}

function UserInfo(props) {
  return (
    <div className="UserInfo">
      <Avatar user={props.user} />
      <div className="UserInfo-name">
        {props.user.name}
      </div>
    </div>
  );
}

function Comment(props) {
  return (
    <div className="Comment">
      <UserInfo user={props.author} />
      <div className="Comment-text">
        {props.text}
      </div>
      <div className="Comment-date">
        {props.date}
      </div>
    </div>
  );
}

function App() {
  return (
    <Comment author={{avatarUrl: "testUrl", name: "TestName"}}
             text="TestText"
             date="15-04-2019"
      />
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
